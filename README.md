# Fake YUKA - Projet de synthèse

Marc, Filoupatir, Léonie, Tuna, Quentin

### Sujet

S’inspirer de Yuka.io pour réaliser une application / site qui permette de contrôler efficacement les achats, en offrant à l’utilisateur des filtres personnalisés et personnalisables (allergies, ratios entre protéines / glucides / lipides, absence de certains conservateurs / additifs). Ces filtres peuvent être mis à disposition des autres. S’appuyer sur l’API de Open Food Facts, et d’autres s’il en existe.

### Architecture

- ./mobile : Application mobile crossplatform - FLUTTER
- ./web : Interface web - REACT
- ./api : Symfony 
